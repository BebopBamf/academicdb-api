#!/bin/sh

# This script is used to create the database and the user for the database

cont = podman

if [ getopts "d:" arg ]; then
    case $arg in
        d)
            cont = docker
            ;;
    esac
fi

case $1 in
    start)
             echo "Creating postgres database with podman"
             $cont run -d \
                 --name academicdb \
                 -e POSTGRES_DB=academicdb \
                 -e POSTGRES_USER=academic \
                 -e POSTGRES_PASSWORD=academic \
                 -p 5432:5432 \
                 postgres:alpine
             echo "Database and user created!"
             ;;

    stop)
                echo "Stopping postgres database"
                $cont stop academicdb
                echo "Database stopped!"
                ;;
esac
