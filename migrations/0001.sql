CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE projects (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    title varchar (255) NOT NULL,
    description varchar text NOT NULL,
    public bool NOT NULL DEFAULT true
);

CREATE TABLE reference_lists (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    project_id uuid NOT NULL REFERENCES projects (id),
    title varchar (255) NOT NUll
);

CREATE TYPE ref_type AS ENUM ('article', 'book', 'inbook', 'proceedings', 'inproceedings', 'preprint', 'misc');

CREATE TABLE refs (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    bibtex_id varchar (63) UNIQUE NOT NULL,
    type ref_type NOT NULL,
    title varchar (255) NOT NULL,
    author varchar (255) NOT NULL,
    year numeric (4) NOT NULL,
    month numeric (2),
    address varchar (254),
    doi varchar (63),
    isbn varchar (63),
    created_at timestamp NOT NULL DEFAULT now(),
    updated_at timestamp NOT NULL DEFAULT now()
);

CREATE TABLE reference_list_refs (
    id uuid PRIMARY KEY,
    ref_id uuid NOT NULL REFERENCES refs (id),
    reference_list_id uuid NOT NULL REFERENCES reference_lists (id)
);

INSERT INTO projects (id, title) VALUES ('a71912d4-50f7-40b1-87be-f0d8dee201eb', 'Default', 'The Default Project');

INSERT INTO reference_lists (id, title, project_id) VALUES ('357c0bb1-4aa8-4137-9e63-5e23d05da2d9','Default','a71912d4-50f7-40b1-87be-f0d8dee201eb')

