{-# LANGUAGE DataKinds #-}

module Api (app, server, runDB) where

import Servant (
    Application,
    HasServer (ServerT),
    Proxy (..),
    Server,
    serve,
 )
import Servant.Server (Handler (..), hoistServer)

import Control.Monad.Reader (MonadIO, ReaderT (runReaderT))

import Core

import Api.Todo

type API = TodoAPI

api :: Proxy API
api = Proxy

server :: (MonadIO m) => ServerT API (AppT m)
server = todoServer

convertApp :: AppContext -> AppT IO a -> Handler a
convertApp s x = Handler $ runReaderT (runApp x) s

appToServer :: AppContext -> Server API
appToServer ctx = hoistServer api (convertApp ctx) server

app :: AppContext -> Application
app = serve api . appToServer
