{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module Config (Environment (..), DBConfig (..), AppConfig (..), makeConnection, configYmlValue, compileTimeConfig) where

import qualified Control.Exception as Exception
import Data.Aeson (Result (..))
import Data.Aeson.Types (fromJSON)
import Data.ByteString (ByteString)
import Data.FileEmbed (embedFile)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8)
import Data.Yaml (
    FromJSON (parseJSON),
    Value,
    decodeEither',
    withObject,
    withText,
    (.:),
 )
import Data.Yaml.Config (applyEnvValue)
import Database.PostgreSQL.Simple (Connection, connectPostgreSQL)
import Network.Wai.Handler.Warp (Port)

data Environment
    = Development
    | Test
    | Production
    deriving (Eq, Show, Read)

data DBConfig = DBConfig
    { dbConfigConnString :: Text
    , dbConfigPoolSize :: Int
    }

data AppConfig = AppConfig
    { appConfigPort :: Port
    , appConfigEnv :: Environment
    , appConfigDB :: DBConfig
    }

instance FromJSON Environment where
    parseJSON = withText "Environment" $ \case
        "development" -> return Development
        "test" -> return Test
        "production" -> return Production
        _ -> fail "Unrecognized environment"

instance FromJSON DBConfig where
    parseJSON = withObject "ConnectInfo" $ \v ->
        DBConfig
            <$> v
            .: "connstr"
            <*> v
            .: "poolsize"

instance FromJSON AppConfig where
    parseJSON = withObject "AppConfig" $ \v ->
        AppConfig <$> v .: "port" <*> v .: "environment" <*> v .: "db"

makeConnection :: AppConfig -> IO Connection
makeConnection = connectPostgreSQL . encodeUtf8 . dbConfigConnString . appConfigDB

configYmlBS :: ByteString
configYmlBS = $(embedFile "config.yaml")

configYmlValue :: Value
configYmlValue =
    either Exception.throw id $
        decodeEither' configYmlBS

compileTimeConfig :: AppConfig
compileTimeConfig =
    case fromJSON $ applyEnvValue False mempty configYmlValue of
        Error e -> error e
        Success cfg -> cfg
