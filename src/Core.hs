{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Core (AppT (..), AppContext (..), runDB, selectDB, insertDB, updateDB, deleteDB) where

import Control.Monad.Except (ExceptT, MonadError)
import Control.Monad.Reader (MonadIO (liftIO), MonadReader, ReaderT, asks)
import Data.Pool (Pool, withResource)
import Data.Profunctor.Product.Default (Default)
import Database.PostgreSQL.Simple (Connection)
import Opaleye (Delete, FromFields, Insert, Select, Update, runDelete, runInsert, runSelect, runUpdate)
import Servant (ServerError)
import System.Log.FastLogger (LoggerSet)

newtype AppT m a = AppT
    { runApp :: ReaderT AppContext (ExceptT ServerError m) a
    }
    deriving
        ( Functor
        , Applicative
        , Monad
        , MonadReader AppContext
        , MonadError ServerError
        , MonadIO
        )

data AppContext = AppContext
    { appContextPool :: Pool Connection
    , appContextLogger :: LoggerSet
    }

runDB :: (MonadIO m) => (Connection -> IO a) -> AppT m a
runDB action = asks appContextPool >>= (\pool -> liftIO $ withResource pool action)

selectDB :: (MonadIO m, Default FromFields fields a) => Select fields -> AppT m [a]
selectDB a = runDB $ \conn -> runSelect conn a

insertDB :: (MonadIO m) => Insert a -> AppT m a
insertDB a = runDB $ \conn -> runInsert conn a

updateDB :: (MonadIO m) => Update a -> AppT m a
updateDB a = runDB $ \conn -> runUpdate conn a

deleteDB :: (MonadIO m) => Delete a -> AppT m a
deleteDB a = runDB $ \conn -> runDelete conn a
