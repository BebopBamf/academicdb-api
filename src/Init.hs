module Init (doMigrations, startApp) where

import Data.Pool (defaultPoolConfig, newPool, withResource)
import Data.Yaml.Config (loadYamlSettings, loadYamlSettingsArgs, useEnv)
import Database.PostgreSQL.Simple (close)
import Database.PostgreSQL.Simple.Migration (MigrationCommand (..), defaultOptions, runMigrations)
import Network.Wai.Handler.Warp (defaultSettings, runSettings, setLogger, setPort)
import Network.Wai.Logger (withStdoutLogger)
import System.Log.FastLogger (defaultBufSize, newStdoutLoggerSet)

import Api (app)
import Config
import Core

migrationsDir :: MigrationCommand
migrationsDir = MigrationDirectory "migrations"

configPath :: FilePath
configPath = "config.yaml"

doMigrations :: IO ()
doMigrations = do
    putStrLn "Running migrations..."
    config <- loadYamlSettings [configPath] [configYmlValue] useEnv
    conn <- makeConnection config
    _ <- runMigrations conn defaultOptions [MigrationInitialization, migrationsDir]
    close conn

startApp :: IO ()
startApp = do
    config <- loadYamlSettingsArgs [configYmlValue] useEnv

    putStrLn $ "Running in " ++ show (appConfigEnv config) ++ " mode..."

    appLogger <- newStdoutLoggerSet defaultBufSize
    connPool <- newPool $ defaultPoolConfig (makeConnection config) close 30 (dbConfigPoolSize $ appConfigDB config)

    putStrLn "Validating migrations..."
    _ <- withResource connPool $ \conn -> runMigrations conn defaultOptions [MigrationValidation migrationsDir]

    let appContext = AppContext connPool appLogger

    putStrLn $ "Listening on port " ++ show (appConfigPort config) ++ "..."
    withStdoutLogger $ \aplogger -> do
        let settings = setPort (appConfigPort config) $ setLogger aplogger defaultSettings
        runSettings settings $ app appContext
