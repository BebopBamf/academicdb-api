{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Models (Todo, TodoForm (..), selectTodo, selectTodoById, insertTodo) where

import GHC.Int (Int32)

import Data.Aeson
import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text (Text)
import Data.Time (LocalTime)
import Opaleye (Field, Insert (..), Select, SqlBool, SqlInt4, SqlText, SqlTimestamp, SqlVarcharN, Table, now, rReturning, selectTable, sqlBool, sqlString, table, tableField, timestamptzAtTimeZone, toFields, where_, (.==))

data TodoForm = TodoForm
    { todoFormName :: Text
    , todoFormDescription :: Text
    }

instance FromJSON TodoForm where
    parseJSON = withObject "TodoForm" $ \v ->
        TodoForm
            <$> v
            .: "name"
            <*> v
            .: "description"

data TodoP a b c d e f = Todo
    { todoId :: a
    , todoName :: b
    , todoDescription :: c
    , todoIsComplete :: d
    , todoCreatedAt :: e
    , todoUpdatedAt :: f
    }

type Todo = TodoP Int32 Text Text Bool LocalTime LocalTime
type TodoWrite = TodoP (Maybe (Field SqlInt4)) (Field SqlVarcharN) (Field SqlText) (Field SqlBool) (Field SqlTimestamp) (Field SqlTimestamp)
type TodoField = TodoP (Field SqlInt4) (Field SqlVarcharN) (Field SqlText) (Field SqlBool) (Field SqlTimestamp) (Field SqlTimestamp)

$(makeAdaptorAndInstance "pTodo" ''TodoP)

todoTable :: Table TodoWrite TodoField
todoTable =
    table
        "todos"
        ( pTodo
            Todo
                { todoId = tableField "id"
                , todoName = tableField "name"
                , todoDescription = tableField "description"
                , todoIsComplete = tableField "is_complete"
                , todoCreatedAt = tableField "created_at"
                , todoUpdatedAt = tableField "updated_at"
                }
        )

instance ToJSON Todo where
    toJSON Todo{..} =
        object
            [ "id" .= todoId
            , "name" .= todoName
            , "description" .= todoDescription
            , "isComplete" .= todoIsComplete
            , "createdAt" .= todoCreatedAt
            , "updatedAt" .= todoUpdatedAt
            ]

    toEncoding Todo{..} =
        pairs
            ( "id"
                .= todoId
                <> "name"
                .= todoName
                <> "description"
                .= todoDescription
                <> "isComplete"
                .= todoIsComplete
                <> "createdAt"
                .= todoCreatedAt
                <> "updatedAt"
                .= todoUpdatedAt
            )

instance FromJSON Todo where
    parseJSON = withObject "Todo" $ \v ->
        Todo
            <$> v
            .: "id"
            <*> v
            .: "name"
            <*> v
            .: "description"
            <*> v
            .: "isComplete"
            <*> v
            .: "createdAt"
            <*> v
            .: "updatedAt"

insertTodo :: TodoForm -> Insert [Int32]
insertTodo form =
    Insert
        { iTable = todoTable
        , iRows =
            [ Todo
                Nothing
                ( toFields
                    . todoFormName
                    $ form
                )
                ( toFields
                    . todoFormDescription
                    $ form
                )
                ( sqlBool
                    False
                )
                nowUTC
                nowUTC
            ]
        , iReturning = rReturning todoId
        , iOnConflict = Nothing
        }
  where
    nowUTC = timestamptzAtTimeZone now $ sqlString "UTC"

selectTodo :: Select TodoField
selectTodo = selectTable todoTable

selectTodoById :: Int32 -> Select TodoField
selectTodoById id_ = do
    todo <- selectTodo
    where_ (todoId todo .== fromIntegral id_)
    pure todo
