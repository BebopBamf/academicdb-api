module Models.ErrorResponse where

data ErrorResponse = ErrorResponse
    { errorResponseTimestamp :: String
    , errorResponseStatus :: Int
    , errorResponseError :: String
    , errorResponseMessage :: String
    , errorResponsePath :: String
    }
    deriving (Show, Eq)
