module Models.Pagination (Pagination (..)) where

data Pagination a = Pagination
    { paginationData :: [a]
    , paginationCount :: Int
    , paginationPage :: Int
    , paginationPerPage :: Int
    }
    deriving (Show, Eq)
