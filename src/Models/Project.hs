{-# LANGUAGE TemplateHaskell #-}

module Models.Project where

import Data.Char (toLower)

-- import Data.Aeson (FromJSON, Options (constructorTagModifier, fieldLabelModifier))
import Data.Aeson.TH
import Data.Text (Text)

data ProjectForm = ProjectForm
    { projectFormTitle :: Text
    , projectFormDescription :: Text
    , projectFormPublic :: Maybe Bool
    }
    deriving (Show)

$(deriveJSON defaultOptions{fieldLabelModifier = drop 7, constructorTagModifier = map toLower} ''ProjectForm)
