module Models.Reference where

import Data.Text (Text)

data ArticleFormData = ArticleFormData
    { afdBibTexID :: Text
    , afdTitle :: Text
    }

data ReferenceForm
    = ArticleForm
    | ProceedingsForm
    | InProceedingsForm

data ReferenceModel
    = Article
    | Proceedings
    | InProceedings
    | Book
    | InBook
    | PrePrint
    | Misc
