{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module Schema where

import Data.Profunctor.Product.TH (makeAdaptorAndInstance)
import Data.Text (Text)
import Data.UUID (UUID)
import Opaleye

data ProjectP a b c d = Project
    { projectId :: a
    , projectTitle :: b
    , projectDescription :: c
    , projectPublic :: d
    }

type Project = ProjectP UUID Text Text Bool
type ProjectWrite = ProjectP (Maybe (Field SqlUuid)) (Field SqlVarcharN) (Field SqlText) (Maybe (Field SqlBool))
type ProjectField = ProjectP (Field SqlUuid) (Field SqlVarcharN) (Field SqlText) (Field SqlBool)

$(makeAdaptorAndInstance "pProject" ''ProjectP)

projectTable :: Table ProjectWrite ProjectField
projectTable =
    table
        "projects"
        ( pProject
            Project
                { projectId = tableField "id"
                , projectTitle = tableField "title"
                , projectDescription = tableField "description"
                , projectPublic = tableField "public"
                }
        )

data ReferenceListP a b c = ReferenceList
    { referenceListId :: a
    , referenceListProjectId :: b
    , referenceListTitle :: c
    }

type ReferenceList = ReferenceListP UUID UUID Text
type ReferenceListWrite = ReferenceListP (Maybe (Field SqlUuid)) (Field SqlUuid) (Field SqlVarcharN)
type ReferenceListField = ReferenceListP (Field SqlUuid) (Field SqlUuid) (Field SqlVarcharN)

$(makeAdaptorAndInstance "pReferenceList" ''ReferenceListP)

referenceListTable :: Table ReferenceListWrite ReferenceListField
referenceListTable =
    table
        "reference_lists"
        ( pReferenceList
            ReferenceList
                { referenceListId = tableField "id"
                , referenceListProjectId = tableField "project_id"
                , referenceListTitle = tableField "title"
                }
        )

data ReferenceP a b c d e f g h i j k l = Reference
    { referenceId :: a
    , referenceBibtexId :: b
    , referenceRefType :: c
    , referenceTitle :: d
    , referenceAuthor :: e
    , referenceYear :: f
    , referenceMonth :: g
    , referenceAddress :: h
    , referenceDoi :: i
    , referenceIsbn :: j
    , referenceCreatedAt :: k
    , referenceUpdatedAt :: l
    }

type Reference = ReferenceP UUID Text Text Text Text Text Int Int Int
